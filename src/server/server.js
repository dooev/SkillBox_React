import express from 'express';

const app = express();

app.get('/', (req, res) => {
    res.send('Hello Di!');
});

app.listen(3000, () => {
    console.log('Server start on http://localhost:3000');
});